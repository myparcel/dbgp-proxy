FROM alpine

ENV IDE_PORT 0.0.0.0:9001
ENV DEBUG_PORT 0.0.0.0:9000

RUN apk --update --no-cache add python py2-pip && \
    pip2 install --upgrade pip && \
    pip2 install --upgrade komodo-python-dbgp

EXPOSE 9000 9001

CMD pydbgpproxy -i ${IDE_PORT} -d ${DEBUG_PORT}
